#include <iostream>
#include <cmath>
#include <cstdint>
#include <cassert>


//###TASK_1#############################################
class Power
{
private:
    double num_1;
    double num_2;

public:
    Power(): num_1(2.5), num_2(5.2)
    {}

    void set(double var_1, double var_2)
    {
        num_1 = var_1;
        num_2 = var_2;
    }
    void calculate()
    {
        std::cout << "Result of Power num_2 from num_1: " << pow(num_1, num_2) << std::endl;    
    }

};



//###TASK_2#############################################
class RGBA {
    
    std::uint8_t m_red = 0;
    std::uint8_t m_green = 0;
    std::uint8_t m_blue = 0;
    std::uint8_t m_alpha = 255;


    RGBA* print(const std::string & name, std::uint8_t value) {
    std::cout << name << " equal " << int(value) << std::endl;
    return this;
  }
    
public:    		
	RGBA(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
		{
			m_red = r;
			m_green = g;
			m_blue = b;
			m_alpha = a;
		} 
    
    void Print()
	{
		std::cout << "r = " << static_cast<int>(m_red) <<
			" g = " << static_cast<int>(m_green) <<
			" b = " << static_cast<int>(m_blue) <<
			" a = " << static_cast<int>(m_alpha) << '\n';
	}

};

//###TASK_3#############################################
class Stack
	{
	private:
		int mem_arr[10];
		int mem_n;

	public:
		void reset()
		{
			mem_n = 0;
			for (int i = 0; i < 10; ++i)
				mem_arr[i] = 0;
		}

		bool push(int value)
		{
			if (mem_n == 10)
				return false;

			mem_arr[mem_n++] = value;
			return true;
		}
	    
        int pop()
	    {
		assert(mem_n > 0);
		return mem_arr[--mem_n];
	    }


		void print()
		{
			std::cout << "( ";
			for (int i = 0; i < mem_n; ++i)
				std::cout << mem_arr[i] << ' ';
			std::cout << ")\n";
		}
	};

int main()
{
    /* ##Task1## */
	{
        std::cout << "################ Task 1 #################" << std::endl;
        Power Task1;
		Task1.set(6.6, 9.9);
		Task1.calculate();
        }
   
    /* ##Task2## */
    {
        std::cout << "\n################ Task 2 #################" << std::endl;
        RGBA* rgba = new RGBA(4, 3, 2, 1);
        rgba->Print();
        delete rgba;
        }
    
    /* ##Task3## */
    {
        std::cout << "\n################ Task 3 #################" << std::endl;
        Stack stack;
        stack.reset();
        stack.print();

        stack.push(3);
        stack.push(7);
        stack.push(5);
        stack.print();

        stack.pop();
        stack.print();

        stack.pop();
        stack.pop();
        stack.print();
        }
        
    return 0;
}
